ansible-role-molecule
=========

A brief description of the role goes here.

Requirements
------------

EPEL (only for EL distros)  
python3  
python3-pip  

Role Variables
--------------

```
molecule_drivers: []
```
You can set any molecule driver you want to install as a list.

**Example:**
```
molecule_drivers: ['molecule[gce]']
```


Dependencies
------------

```
geerlingguy.repo-epel
geerlingguy.pip
```

Example Playbook
----------------

```yaml
- hosts: all
  vars:
    pip_package: python3-pip
    molecule_drivers: ['molecule[gce]']
  tasks:
    - name: Installing EPEL
      include_role: 
        name: geerlingguy.repo-epel
      when: ansible_facts['os_family'] == 'RedHat'

    - name: Installing python3
      package: 
        name: python3
      become: yes

    - name: Installing Pip
      include_role:
        name: geerlingguy.pip

    - name: Installing Molecule
      roles:
        - ansible-role-molecule
```

License
-------

Apache2

Author Information
------------------

Jonathan Scherrer  
jonathan.x.scherrer@gmail.com
